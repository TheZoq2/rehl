use structopt::StructOpt;
use regex::Regex;

use std::io::{ErrorKind, Error};

#[derive(StructOpt, Debug)]
#[structopt
    (name = "rehl"
    , about = r#"
Highlights parts of stdin based on regexes. Each colour supports
multiple space spearated patterns.

EXAMPLES:
Highlight errors and squiggly lines with red
> rehl -r Error: "\^~*"

Also highlight warnings with yellow
> rehl -r Error: "\^~*" -y Warning:
"#)]
struct Opt {
    #[structopt(short, long)]
    red: Vec<String>,
    #[structopt(short, long)]
    blue: Vec<String>,
    #[structopt(short, long)]
    yellow: Vec<String>,
    #[structopt(short, long)]
    gray: Vec<String>,
    #[structopt(short, long)]
    purple: Vec<String>,
}

fn hl_line(re: &Regex, color_code: (i32, i32), line: &str ) -> String {
    let replacement: &str = &format!(
        "\x1b[{};{}m$0\x1b[0m", color_code.0, color_code.1
    );
    re.replace_all(&line, replacement)
        .into()
}

macro_rules! generate_regexes {
    ($opt:ident -> $target:ident [$( ($arg:ident, $color:expr) ),*]) => {
        let mut $target = vec![];
        $(
        for regex in $opt.$arg {
            $target.push((Regex::new(&regex).unwrap(), $color));
        }
        )*
    }
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    generate_regexes!(opt -> regexes [
        (red, (0, 31i32)),
        (blue, (0, 34)),
        (yellow, (1, 33)),
        (gray, (0, 37)),
        (purple, (0, 35))
    ]);

    loop {
        let mut line = String::new();
        match std::io::stdin().read_line(&mut line) {
            Ok(0) => {
                break Ok(())
            }
            Ok(_) => {
                let result = regexes.iter()
                    .fold(line, |line, (re, code)| {
                         hl_line(&re, *code, &line)
                    });
                print!("{}", result);
            }
            Err(e) => {
                match e.kind() {
                    ErrorKind::WouldBlock => {
                        continue;
                    }
                    ErrorKind::BrokenPipe => {
                        break Ok(());
                    }
                    _ => {
                        return Err(e)
                    }
                }
            }
        }
    }
}
