# Reglx Highlighter (rehl)

Add highlighting to stdin for easier to read compiler output, or more readable
log files.

## Screenshots

Improve compiler output readability

![Compiler output example](screenshots/verilator.png "Compiler output example")

Or get a better overview of log files

![Logfile example](screenshots/xorg_log.png "Logfile example")


## Installation

rehl is written in rust and can be installed using cargo

```
cargo install rehl
```

## Usage

The patterns to highlight are specified using colour flags followed by one or more
regex patterns separated by spaces. Any text passed through stdin will have the
colour applied to text matching those patterns.

```bash
# To  errors and squiggly lines with red
> rehl -r Error: "\^~*"

# Also highlight warnings with yellow
> rehl -r Error: "\^~*" -y Warning:
```

See `rehl --help` for more information


